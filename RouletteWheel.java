import java.util.Random;
public class RouletteWheel {
    private Random rand;
    private int number;

    public RouletteWheel(){
        this.number = 0;
        rand = new Random();
    }
    public void spin() {
        this.number = rand.nextInt(38);
    }

    public int getValue(){
        return this.number;
    }

    public boolean isLow(){
        if(this.number >= 18 && this.number <= 1){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean isHigh(){
        if(this.number >= 19 && this.number <= 36){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean isEven(){
        if((this.number) % 2 == 1 || this.number == 0){
            return false;
        }
        else{
            return true;
        }
    }

    public boolean isOdd(){
        if(this.number % 2 == 1){
            return true;
        }
        else{
            return false;
        }
    }
}
