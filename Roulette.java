import java.util.Scanner;

public class Roulette{
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        RouletteWheel gamble = new RouletteWheel();

        //default money is 50
        int money = 50;

        System.out.println("Would you like to bet ?\n (Y/N)");
        char answer = read.nextLine().charAt(0);
        while(answer == 'y' || answer == 'Y'){
            System.out.println("Which number would you like to bet on");
            int betNumber = read.nextInt(); 

            System.out.println("How much would you like to bet?");
            int bet = read.nextInt(); 
            gamble.spin();

            System.out.println("The ball landed on "+ gamble.getValue());
            if(betNumber == gamble.getValue()){
                System.out.println("Congrats you've won "+bet*35+"$");
                money = money +(bet*35);
                System.out.println("Your total amount of cash is " + money);

            }
            else{
                System.out.println("Sorry pal better luck next time\n");
                money = money-bet;
                System.out.println("You have "+money+"$ still remaining");
                
            }
        }
        
        System.out.println("Please exit the casino");
        
        
    }
}